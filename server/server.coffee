express = require('express')
mongoose = require('mongoose')
path = require('path')
fs = require('fs')
crypto = require('crypto')
state = crypto.randomBytes(8).toString('hex')
request = require('request')
hbs = require('hbs')
_ = require('underscore')
async = require('async')

crawler = require('./crawler')

app = express()
env = app.get('env')
config = require('./config')[env]

mongoose.connect('mongodb://localhost/dict')

db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
PER_PAGE = 10

DictDataSchema = new mongoose.Schema(
  access_token: String
  rank: Number
  vocabulary: {type: String, unique: true, required: true},
  sentences: mongoose.Schema.Types.Mixed,
  data: mongoose.Schema.Types.Mixed
)

UserDataSchema = new mongoose.Schema(
  name: {type: String, required: true}
  access_token: {type: String, required: true}
)


DictData = mongoose.model('DictData', DictDataSchema)
FreqData = mongoose.model('FreqData', new mongoose.Schema)
UserData = mongoose.model('UserData', UserDataSchema)
publicFolder = path.resolve(__dirname + '/../public')
templatesFolder = path.resolve(publicFolder + '/templates')
partialFolder = path.resolve(templatesFolder + '/partials')


# register hbs partial
files = fs.readdirSync(partialFolder)
files.forEach( (file) ->
  match = /^([^.]+).hbs$/.exec(file)
  return false if not match
  filename = match[1]
  template = fs.readFileSync(partialFolder + '/' + file, 'utf-8')
  hbs.registerPartial(filename, template)
)

# register hbs helper
hbs.registerHelper('eq', (v1, v2, options) ->
  if (v1 is v2)
    return options.fn(this)
  return options.inverse(this)
)

hbs.registerHelper('debug', (optionalValue) ->
  console.log("Current Context")
  console.log("====================")
  console.log('this', this)
 
  if (optionalValue)
    console.log("Value")
    console.log("====================")
    console.log(optionalValue)
)

hbs.registerHelper('inline', (where) ->
  content = fs.readFileSync(path.resolve(publicFolder + where), 'utf-8')
  return content
)


hbs.registerHelper "linkto", (rank) ->
  page = Math.max(Math.ceil(rank / PER_PAGE) - 1, 0)
  from = page * PER_PAGE + 1
  to = from + PER_PAGE - 1
  from + "-" + to

hbs.registerHelper "encode", (str) ->
  encodeURIComponent(str)


app.use(express.static(publicFolder))
app.use(express.urlencoded())
app.use(express.json())
app.use(express.cookieParser())
app.use(express.session(secret: "sTebr5sa?atr"))
app.set('view engine', 'htm')
app.engine('htm', hbs.__express)

# allow CORS with customize header 'X-Requested-With: XMLHttpRequest'
app.options('/list/page/:page', (req, res) ->
  res.header 'Access-Control-Allow-Origin', '*'
  res.header 'Access-Control-Allow-Credentials', true
  res.header 'Access-Control-Allow-Methods', 'GET, POST, OPTIONS, DELETE'
  res.header 'Access-Control-Allow-Headers', 'X-Requested-With'
  res.header 'Access-Control-Max-Age', '1728000'
  res.header 'Content-Length', '0'
  res.send(200)
)

app.get('/auth/github', (req, res) ->
  url = "https://github.com/login/oauth/authorize?client_id=#{config.client_id}&scope=user,user:email&state=#{state}&redirect_uri=#{config.redirect_uri}"
  res.redirect(url)
)

app.get('/auth/github/callback', (req, res) ->
  code = req.query.code
  u = "https://github.com/login/oauth/access_token?client_id=#{config.client_id}&client_secret=#{config.client_secret}&code=#{code}&state=#{state}"
  request.get({url:u, json: true},  (err, rsp, body) ->
    if err
      cosnole.log 'error', err
    else
      access_token = body.access_token
      res.cookie('access_token', access_token)
      res.redirect('/')
  )
)

app.get('/signout', (req, res) ->
  res.clearCookie('connect.sid')
  res.clearCookie('access_token')
  res.redirect('/')
)

app.get('/list/page/:page', (req, res) ->
  access_token = req.cookies.access_token
  page = +req.params.page
  sortBy = req.query.sortBy or 'time'
  if sortBy is 'alphabetically'
    sortBy = 'vocabulary'
  if sortBy is 'time' or ''
    sortBy = '-_id'

  query = DictData.find(access_token: access_token)

  if sortBy is 'random'
    query.select('-access_token -_id -__v').exec((err, docs) ->
      DictData.count(access_token: access_token, (err, count) ->
        res.header 'Access-Control-Allow-Origin', '*'
        res.set('Content-Type', 'application/json; charset=utf-8')
        res.send(docs: _.sample(docs, PER_PAGE), sortBy: sortBy, page:page, total: count, pages: Math.ceil(count/PER_PAGE))
      )
    )
  else

    query.sort(sortBy).limit(PER_PAGE).skip(PER_PAGE * page).select('-access_token -_id -__v').exec((err, docs) ->
      DictData.count(access_token: access_token, (err, count) ->
        res.header 'Access-Control-Allow-Origin', '*'
        res.set('Content-Type', 'application/json; charset=utf-8')
        res.send(docs: docs, sortBy: sortBy, page:page, total: count, pages: Math.ceil(count/PER_PAGE))
      )
    )
)


# get rank by vocabulary
app.get('/rank/:rank', (req, res) ->
  rank = req.params.rank

  split = rank.split('-')
  from = +split[0]
  to = +split[1]
  # http://dict.chunghe.me/rank/hello
  if (isNaN(from) and isNaN(to))
    FreqData.findOne(v: rank, (err, doc) ->
      if doc
        res.json(doc)
      else
        res.send(404, 'Not Found')
    )
  # http://dict.chunghe.me/rank/15921-15930
  else
    nextPageUrl = "/rank/#{from + PER_PAGE}-#{to + PER_PAGE}"
    prevPageUrl = "/rank/#{from - PER_PAGE}-#{to - PER_PAGE}"

    FreqData.find().sort(rank: 1).limit(PER_PAGE).skip(from - 1).select('-_id -__v').lean().exec((err, docs) ->
      res.render(publicFolder + '/rank.htm', {docs: docs, prevPageUrl: prevPageUrl, nextPageUrl: nextPageUrl})
    )
)

app.delete('/delete/:vocabulary', (req, res) ->
  vocabulary = req.params.vocabulary
  access_token = req.cookies.access_token
  DictData.remove(access_token: access_token, vocabulary: vocabulary, (err) ->
    if err
      res.send(500, err.message)
    else
      res.send(200, {vocabulary: vocabulary})
  )
)

app.get('/', (req, res) ->
  access_token = req.cookies.access_token
  if access_token
    res.render(publicFolder + '/index.htm', {loggedIn: true, access_token: access_token, sortBy: req.cookies.sortBy})
  else
    res.render(publicFolder + '/login.htm')
)

app.get('/new', (req, res) ->
  if not req.cookies.access_token
    res.redirect('/')
    return false
  if _.size(req.query) is 0
    res.render(publicFolder + '/new.htm')
  else
    # http://dict.chunghe.me/new?type=video&source=thor
    doc = {sentences: [{}]}
    doc.sentences[0].type = req.query.type
    doc.sentences[0].source = req.query.source
    res.render(publicFolder + '/new.htm', {doc: doc})
)

app.get('/sources', (req, res) ->
  access_token = req.cookies.access_token
  if not access_token
    res.redirect('/')
    return false
  DictData.find(access_token: access_token).select('sentences -_id').sort('-_id').exec((err, doc) ->
    if err
      res.send(500, err.message)
    else if doc is null
      res.send(404, 'Not Found')
    else
      pluck = _.pluck(doc, 'sentences')
      flatten = _.flatten(pluck)
      data =  _.countBy(_.pluck(flatten, 'source'), (item) ->
        return item
      )
      res.render(publicFolder + '/sources.htm', {data: data})
  )
)

app.get(/\/source\/([^?]*)/, (req, res) ->
  access_token = req.cookies.access_token
  if not access_token
    res.redirect('/')
    return false
  source = req.params[0]
  DictData.find(access_token: access_token, sentences: {$elemMatch: {source: source}}).select('-_id -__v').exec((err, docs) ->
    res.render(publicFolder + '/source.htm', {source: source, docs: JSON.stringify(docs)})
  )
)

app.get('/edit/:vocabulary', (req, res) ->
  access_token = req.cookies.access_token
  if not access_token
    res.redirect('/')
    return false
  vocabulary = req.params.vocabulary
  
  DictData.findOne(access_token: access_token, vocabulary: vocabulary, (err, doc) ->
    if err
      res.send(500, err.message)
    else if doc is null
      res.send(404, 'Not Found')
    else
      res.render(publicFolder + '/new.htm', {doc: doc})
  )
)

app.get('/v/:vocabulary', (req, res) ->
  access_token = req.cookies.access_token
  if not access_token
    res.redirect('/')
    return false
  vocabulary = req.params.vocabulary
  isMobile = req.headers['user-agent'].indexOf('Mobile') > -1

  DictData.findOne(access_token: access_token, vocabulary: vocabulary).exec( (err, doc) ->
    if (doc is null)
      res.send(404, 'Not found')
    else
      sortBy = req.cookies.sortBy or 'time'
      if sortBy is 'alphabetically'
        sortBy = 'vocabulary'
      if sortBy is 'time' or ''
        sortBy = '-_id'
      
      if sortBy is 'random'
        DictData.find().select('-_id -__v').exec( (err, doc) ->
          res.render(publicFolder + '/vocabulary.htm', {doc: JSON.stringify(_.shuffle(doc)), vocabulary: vocabulary})
        )
      else
        DictData.find().sort(sortBy).select('-_id -__v').exec( (err, doc) ->
          res.render(publicFolder + '/vocabulary.htm', {doc: JSON.stringify(doc), vocabulary: vocabulary, isMobile: isMobile})
        )
  )
)

getFreq = (vocabulary, callback) ->
  FreqData.findOne({v: vocabulary}).lean().exec( (err, doc) ->
    rank = if doc then doc.rank else 99999
    callback(rank)
  )

upsert = (payload, type, callback) ->

  vocabulary = payload.vocabulary.trim()
  access_token = payload.access_token
  if Array.isArray(payload.sentences)
    payload.sentences.forEach( (s) ->
      s.content = s.content.trim()
      s.source = s.source.trim()
    )
  sentences = payload.sentences
  DictData.findOne({vocabulary: vocabulary}, (err, doc) ->
    if doc
      if type is 'new'
        tosave = access_token: access_token, vocabulary: vocabulary, sentences: doc.sentences.concat(sentences)
      else
        tosave = access_token: access_token, vocabulary: vocabulary, sentences: sentences

      DictData.findOneAndUpdate({vocabulary: vocabulary}, tosave, (err, doc) ->
        if err
          console.log "error while saving/updating #{vocabulary}", err
        else
          callback(null, vocabulary)
      )
    # first search with `ferring`, not found -> 
    # craw data, so if the payload contains 'data' field, no need to craw again
    # in this case, could always create a new item
    else if payload.data
      dictData = new DictData(payload)
      dictData.save((err, doc) ->
        if err
          callback(500, err.err)
        else
          callback(null, vocabulary)
      )
    else
      crawler.fetch(vocabulary, (data) ->
        if (data.vocabulary)
          getFreq(data.vocabulary, (rank) ->
            payload =
              access_token: access_token
              rank: rank
              data: data
              vocabulary: data.vocabulary
              sentences: payload.sentences

            upsert(payload, type, callback)
          )
        else
          callback(404, 'not found')
      )
  )


# if vocabulary exists, merge exists
# else fetch and save
app.post('/new', (req, res) ->
  payload =
    vocabulary: req.body.vocabulary
    sentences: req.body.sentences
    access_token: req.cookies.access_token
  upsert(payload, 'new', (err, result) ->
    if err
      res.send(500, err)
    res.send(result)
  )
)

# save whatever it posts
app.post('/edit', (req, res) ->
  payload =
    vocabulary: req.body.vocabulary
    sentences: req.body.sentences
  upsert(payload, 'edit', (err, result) ->
    if err
      res.send(500, err)
    res.send(result)
  )
  
)

port = 3003
app.listen(port, 'localhost')
console.log "listening on port #{port}"
