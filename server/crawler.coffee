
request = require 'request'
cheerio = require 'cheerio'
_ = require 'underscore'

fetch = (q, callback) ->
  url = "http://tw.dictionary.search.yahoo.com/search?p=#{q}&fr2=dict"
  request(url, (error, rsp, body) ->
    $ = cheerio.load(body)

    data = {}

    # data.vocabulary
    vocabulary = $('.title_term .yschttl').text()
    data.vocabulary = vocabulary
      
    # data.pronun
    $pronun = $('.proun_wrapper')
    type = $pronun.find('.proun_type').eq(0).text()
    value = $pronun.find('.proun_value').eq(0).text()
    data.pronun = {}
    data.pronun.type = type
    data.pronun.value = value

    # data.sound_file
    sound_file = $pronun.find('.proun_sound a').attr('href')
    data.sound_file = sound_file

    # data.explanation
    $sections = $('.result_cluster_first .explanation_pos_wrapper')
    data.explanation = []
    $sections.each( (i, section) ->
      data.explanation[i] = {} if not data.explanation[i]
      $section = $(section)
      $hd = $section.find('.explanation_group_hd')
      abbr = $hd.find('.pos_abbr').text()
      desc = $hd.find('.pos_desc').text()
      $exp_item = $section.find('.explanation_ol li')
      data.explanation[i].abbr = "#{abbr} #{desc}"
      $exp_item.each( (index, item) ->
        data.explanation[i].exps = [] if _.isEmpty(data.explanation[i].exps)
        $item = $(item)
        exp = $item.find('.explanation').text()
        $samples = $item.find('.sample')
        samples = []
        if $samples.length > 0
          $samples.each((sample_index, sample) ->
            samples.push($(sample).text())
          )
        # console.log 'sample',sample
        data.explanation[i].exps[index] = {} if not data.explanation[i].exps[index]
        data.explanation[i].exps[index].exp = exp
        data.explanation[i].exps[index].samples = samples if samples.length
      )
    )
    callback(data)
  )
  ###
  data = {"pronun":{"type":"KK","value":"[ˋkʌlmə͵net]"},"sound_file":"http://l.yimg.com/tn/dict/kh/v1/31847.mp3","explanation":[{"abbr":"vi. 不及物動詞","exps":[{"exp":"達到最高點; 達到高潮; 告終[(+in)]","samples":["culminate in bankruptcy 以破產告終","The battle culminated in total victory. 這一仗大獲全勝。"]},{"exp":"(天體)到子午線"}]},{"abbr":"vt. 及物動詞","exps":[{"exp":"使達最高點(或高潮)","samples":["Their marriage culminated their long friendship. 他倆交友有年, 最後終成眷屬。"]},{"exp":"使結束"}]}]}
  callback(data)
  ###


exports.fetch = fetch
