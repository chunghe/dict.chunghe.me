
excel = require('excel')
mongoose = require('mongoose')
fs = require('fs')

mongoose.connect('mongodb://localhost/dict')

db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))

FreqSchema = new mongoose.Schema(
  rank: Number,
  v: {type: String},
  pos: {type: String}
)
FreqData = mongoose.model('FreqData', FreqSchema)


dataFolder = 'data/'
files = fs.readdirSync(dataFolder)
#files = ['1.to.5000.xlsx', '5001.to.10000.xlsx']
console.log 'files', files

importExcelFile = (path) ->
  console.log 'importing', path
  excel(path, (err, rows) ->
    console.log 'rows', rows.length
    rows.forEach((row, index) ->
      if index > 0
        rank = row[0]
        pos = row[1]
        v = row[2].trim().replace(/^\(|\)$/g, '').toLowerCase()
        freqData = new FreqData(rank: rank, v: v, pos: pos)
        freqData.save((err, doc) ->
          console.log err.err if err
        )
    )
  )


files.forEach((file) ->
  path = "#{dataFolder}#{file}"
  importExcelFile(path)
)

