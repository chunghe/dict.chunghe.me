install
-------------
1. cd server; npm install
2. cd public; bower install

start service
-------------
cake run

TODO
-------------
- [x] able to play sound in list page
- [x] implement popover in list page
- [x] use partial template to reuse new/edit page
- [x] able to display/store two sample senteces
- [x] able to edit single vocabulary
- [x] better warning message for duplicate key
- [x] popover should not too close the edge of the screen
- [x] source page to list all the sources
- [x] reuse sample layout for index/sources/souce page
- [x] move BE template to be template folder
- [x] word frequency
- [x] implement pagination in list page
- [x] implement sort by menu
- [x] in single vocabulary page, should be able to navigate to next item per sort method
- [x] remember soryBy information (store in the cookie)
- [x] able to navigate to index page in every page
- [x] implement bulk importer
- [x] hotkey support (zxcvf)
- [x] able to jump to the last page
- [x] in single vacaburlary page, should be able to delete a sentence
- [x] before import new items, make sure there's no duplicate sentences
- [x] should store the vocabulary fetch from crawler instead of user input (ex: auditoriums -> auditorium)
- [x] display rank in single vocabulary page 
- [x] for mobile browser, should be able to swipe left/right to view prev/next page
- [x] sort by random
- [x] better url for rank page, change from /rank/8058 to /rank/8051-8060
- [x] able to navigate in rank page
- [x] add totoal count  information at the footer
- [ ] cron job to backup dict dats
- [ ] able to do 'dcit clash --save', then could login github account and save to the web
- [ ] handle input vocabulary not exist
- [ ] 'add more to this source' in souce page
- [ ] sign in with github
- [ ] listing page to list top 20000 most used words
- [ ] text to speach, http://techcrunch.com/2009/12/14/the-unofficial-google-text-to-speech-api/
- [ ] able to export/import data

BUG
-------------
- [x] fix placement of popover

WORD FEQUENCY
-------------
- http://www.wordfrequency.info/
- http://corpus2.byu.edu/coca/files/100k_samples.txt
- http://ucrel.lancs.ac.uk/bncfreq/flists.html
- http://corpus.byu.edu/coca/ 
- http://blog.roodo.com/victoracademy/archives/12451639.html
單字數量            可讀懂得範圍        
  1,000                 72%        
  2,000                 79.7%        
  3,000                 84.0%        
  4,000                 86.8%        
  5,000                 88.7%        
  6,000                 89.9%
 15,851                 97.8%
- https://github.com/search?q=english+word+frequency&type=Repositories&ref=searchresults
- http://norvig.com/ngrams/
- http://googleresearch.blogspot.tw/2006/08/all-our-n-gram-are-belong-to-you.html 
- http://www.lextutor.ca/research/nation_waring_97.html
At present the best conservative rule of thumb that we have is that up to a vocabulary size of around 20,000 word families, we should expect that native speakers will add roughly 1000 word families a year to their vocabulary size. That means that a five year old beginning school will have a vocabulary of around 4000 to 5000 word families. A university graduate will have a vocabulary of around 20,000 word families (Goulden, Nation and Read, 1990). These figures are very rough and there is likely to be very large variation between individuals. These figures exclude proper names, compound words, abbreviations, and foreign words. A word family is taken to include a base word, its inflected forms, and a small number of reasonably regular derived forms (Bauer and Nation, 1993). Some researchers suggest vocabulary sizes larger than these (see Nagy, this volume), but in the well conducted studies (for example, D'Anna, Zechmeister nad Hall, 1991) the differences are mainly the result of differences in what items are included in the count and how a word family is defined.


- http://www.talkenglish.com/Vocabulary/english-vocabulary.aspx
There are roughly 100,000 word-families in the English language.
A native English speaking person knows between 10,000 (uneducated) to 20,000 (educated) word families.
Professor Paul Nation found that a person needs to know 8,000-9,000 word families to enjoy reading a book.
Studying heritage language learners reveal that a person with a vocabulary size of 2,500 passive word-families and 2,000 active word-families can speak a language fluently.
- http://answers.google.com/answers/threadview/id/752639.html

- http://en.wiktionary.org/wiki/Wiktionary:Frequency_lists
- http://www.wordfrequency.info/purchase.asp

